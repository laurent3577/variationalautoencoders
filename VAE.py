import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np 
from tqdm import tqdm
from scipy.misc import imresize
import matplotlib.pyplot as plt


class Reshape(nn.Module):
	""" Wrapper for reshaping operation.
		Can be added to sequential modules to enable
		reshaping without redefining the forward pass.
	"""
    def __init__(self, *args):
        super(Reshape, self).__init__()
        self.shape = args

    def forward(self, x):
        return x.view(x.shape[0], *self.shape)


class CVAE(nn.Module):
	def __init__(self, n_latent, encoder, decoder, conditioning=True):
		""" 
			Params
			----------
			n_latent : (int) Latent variable dimension
			encoder : (nn.Module) Module encoding data to its latent code
			decoder : (nn.Module) Module decoding code to data
			conditioning : (bool) Whether model uses conditioning or not
		"""
		super(CVAE, self).__init__()
		self.n_latent = n_latent
		self.conditioning = conditioning
		self.encoder = encoder
		self.decoder = decoder

		# Output of encoder is then processed by two different layers to produce 
		# encoding for mean and log variance of the Gaussian modeling the latent code
		try:
			encoder_out_dim = list(encoder.children())[-1].shape[0]
		except:
			encoder_out_dim = list(encoder.children())[-2].out_features
		self.mean_encoder = nn.Linear(encoder_out_dim, self.n_latent)
		self.logvar_encoder = nn.Linear(encoder_out_dim, self.n_latent)
		

	def get_batches(self, X, labels, batch_size):
		""" 
			Shuffles data and compute batches
		"""
		permute = np.random.permutation(X.shape[0])
		X = X[permute]
		labels = labels[permute]
		batches = []
		for k in range(len(X)//batch_size):
			batches.append((X[k*batch_size: (k+1)*batch_size], labels[k*batch_size: (k+1)*batch_size]))
		return batches


	def train(self, X, labels, epoch=10, batch_size=64, lr=1e-3, test_every=20):
		"""
			Params
			----------
			X : (array) training data
			labels : (array) training labels
			epoch : (int) number of epochs
			batch_size : (int) size of batches
			lr : (float) learning rate
			test_every : (int) interval between producing sample during training
		"""
		optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=5e-4)
		it = 1
		for e in range(epoch):
			batches = self.get_batches(X, labels, batch_size)
			pbar = tqdm(batches)
			for batch in pbar:
				x, y = batch
				x_init = torch.FloatTensor(x)

				if self.conditioning:
					cond_vec = torch.FloatTensor(y)
					if x.dim() == 4:
						cond_map = np.zeros((x.shape[0], y.shape[1], x.shape[2], x.shape[3]))
						cond_map[np.arange(x.shape[0]), np.argmax(y, axis=1),:,:] = 1
						x = torch.FloatTensor(np.concatenate((x, cond_map), axis=1))
					elif x.dim() == 2:
						x = torch.cat((torch.FloatTensor(x), cond_vec), 1)
				
				mu = self.mean_encoder(self.encoder(x))
				logvar = self.logvar_encoder(self.encoder(x))
				std = torch.exp(0.5*logvar)

				eps = torch.randn_like(std)

				z = mu + std * eps
				if self.conditioning:
					z = torch.cat((z,cond_vec), 1)

				rec = self.decoder(z)

				rec_loss = F.binary_cross_entropy(rec, x_init.view(rec.shape), reduction='sum') # maximize E[log P(X|z)]
				kld_loss = 0.5*torch.sum(-1. - logvar + mu.pow(2) + logvar.exp())
				loss = rec_loss + kld_loss # maximize -kld is equivalent to minimize kld

				loss.backward()
				optimizer.step()
				optimizer.zero_grad()
				pbar.set_description('Train Epoch : {0}/{1} Loss : {2:.4f} '.format(e+1, epoch, float(loss.data)))
				it += 1
				if it%test_every==0:
					test = rec.detach().numpy().squeeze(1)[0]
					test = imresize(test, (200,200))
					plt.imsave('TrainingSamples/training_sample_{}.png'.format(it), test, cmap='gray')

	def get_samples(self, n_samples, cond=None):
		"""
			Samples the estimated distribution.

			Params
			----------
			n_samples : (int) number of samples to produce
			cond : (array) conditionning vector (if data is 1d) or feature map (id data is 2d) 
					for each sample to produce.
		"""
		if self.conditioning:
			assert cond is not None, "Conditioning required"
			z = torch.cat((torch.FloatTensor(np.random.multivariate_normal(np.zeros(self.n_latent), np.eye(self.n_latent), n_samples)), torch.FloatTensor(cond)),1)
		else:
			z = torch.FloatTensor(np.random.multivariate_normal(np.zeros(self.n_latent), np.eye(self.n_latent), n_samples))

		return self.decoder(z).detach().numpy().squeeze(1)

	def save_samples(self, n_samples, cond=None):
		"""
			Samples and saves as png files.
		"""
		samples = self.get_samples(n_samples,cond)
		i = 0
		for im in samples:
			i += 1
			im = imresize(im, (200,200))
			plt.imsave('GeneratedSamples/generated_sample_{}.png'.format(i), im, cmap='gray')
