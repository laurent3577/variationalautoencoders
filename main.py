from VAE import *
import mnist


n_latent = 10
## Fully connected encoder / decoder
encoder = nn.Sequential(
						nn.Linear(784+10,512),
						nn.ReLU(True),
						nn.Linear(512,256),
						nn.ReLU(True)
						)
decoder = nn.Sequential(
						nn.Linear(n_latent+10, 256),
						nn.ReLU(True),
						nn.Linear(256,512),
						nn.ReLU(True),
						nn.Linear(512,784),
						nn.Sigmoid(),
						Reshape(1,28,28)
						)

## Convolutionnal encoder / decoder

# encoder = nn.Sequential(
# 					nn.Conv2d(1,16, kernel_size=3, stride=1, padding=1),
# 					nn.ReLU(True),
# 					nn.MaxPool2d(kernel_size=2, stride=2),
# 					nn.Conv2d(16,32, kernel_size=3, stride=1, padding=1),
# 					nn.ReLU(True),
# 					Reshape(32*14*14))

# decoder = nn.Sequential(
# 					nn.Linear(n_latent, 256),
# 					nn.ReLU(True),
# 					nn.Linear(256, 14*14),
# 					nn.ReLU(True),
# 					nn.Dropout(p=0.3),
# 					Reshape(1,14,14),
# 					nn.ConvTranspose2d(1,16, kernel_size=3, stride=1, padding=1),
# 					nn.ReLU(True),
# 					nn.ConvTranspose2d(16,16, kernel_size=3, stride=1, padding=1),
# 					nn.ReLU(True),
# 					Reshape(16*14*14),
# 					nn.Linear(16*14*14, 28*28),
# 					nn.Sigmoid(),
# 					Reshape(1,28,28))

data = mnist.train_images()
labels = mnist.train_labels()
onehot_labels = np.zeros((len(labels),10))
onehot_labels[np.arange(len(labels)),labels] = 1
data = np.expand_dims(data, axis=1)
data = data/255.
data = torch.FloatTensor(data)
data = data.view(data.shape[0],-1)	
model = CVAE(n_latent, encoder, decoder, conditioning=True)
model.train(data, onehot_labels , epoch=10)

# Test generating samples
n_samples = 15
cond = np.zeros((n_samples*10,10))
for k in range(10):
	cond[k*n_samples:(k+1)*n_samples,k]=1
model.save_samples(n_samples*10, cond=cond)